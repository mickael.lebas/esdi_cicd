#!/bin/bash

# Obtenir les valeurs
login="$1"
password="$2"

# Check #1 : différent du login
if [ "$login" = "$password" ] ; then
    echo "[!] Password same as login"
    exit 1
# Check #2 : non nul
elif [ -z "$password" ] ; then
    echo "[!] Password is null"
    exit 1
# Check #3 : différent de password
elif [ "$password" = "password" ] ; then
    echo "[!] Password is password"
    exit 1
# Check #4 : différent de motdepasse
elif [ "$password" = "motdepasse" ] ; then
    echo "[!] Password is motdepasse"
    exit 1
# Check 5 : seulement des chiffres
elif [ "$password" -eq "$password" ] 2> /dev/null ; then
    echo "[!] Password contains only digits"
    exit 1
fi

dictionary_file="rockyou.txt"

grep -q "$password" ./"$dictionary_file" && { echo "[!] Password is too common. please choose another one"; exit 1; }

# Hacher le mot de passe
hash=$(mkpasswd -m sha-512 "$password")

# Ajouter l'utilisateur
useradd -m -p "$hash" -s /bin/bash "$login"
