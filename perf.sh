#!/bin/bash

update_cpu_stats() {
    local cpu_usage=$(ps -p $$ -o %cpu | awk 'NR==2 {print $1}')
    echo "CPU Usage: $cpu_usage%"
}

update_disk_stats() {
    local disk_usage=$(df --output=pcent / | tail -n 1 | tr -d '%')
    echo "Disk Usage: $disk_usage%"
}

update_mem_stats() {
    local mem_usage=$(free | awk 'NR==2 {print $3/$2 * 100}')
    echo "Memory Usage: $mem_usage%"
}

start_time=$(date +%s)

./builds/mickael.lebas/esdi_cicd/exo3.sh mickael "apKhfft25_*"

update_cpu_stats
update_disk_stats
update_mem_stats

end_time=$(date +%s)
total_time=$((end_time - start_time))

echo "Total Execution Time: $total_time seconds"

exit 0
